import React, {Component} from "react"
import { useRouter } from 'next/router'
import { Button, FormControl, InputGroup } from "react-bootstrap"
import axios from "axios"

export default class extends Component {
    constructor(props){
        super(props)
        this.state = {country:"", name:"", lat:"", lng:""}
    }

    static getInitialProps({query}) {
        return {query}
    }

    componentDidMount(){
        const {country, name, lat, lng } = this.props.query
        this.setState({country, name, lat, lng })
    }

    render() {
        return (
            <div className="container">
                <h1>Weather Report</h1>
                <h2>{this.state.name} - {this.state.country}</h2>
                <img
                    alt={this.state.country}
                    style={{height: "30%", width: "30%"}}
                    src={`http://catamphetamine.gitlab.io/country-flag-icons/3x2/${this.state.country}.svg`}
                />
            </div>
        )
    }
}