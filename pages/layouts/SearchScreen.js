import React, {Component} from "react"
import { Button, FormControl, InputGroup } from "react-bootstrap"
import cities from 'cities.json';
import axios from "axios"
import PlaceCard from "./PlaceCard"

export default class extends Component {
    constructor(props){
        super(props)
        this.state = {address: "", places: []}
        this.addressChange = this.addressChange.bind(this)
        this.search = this.search.bind(this)
    }
    
    addressChange(event){
        this.setState({address: event.target.value})
    }

    async search(){/*
        const data = await axios.get(`http://api.positionstack.com/v1/forward?access_key=ea1e17d4f469c4bbe3321a9dfad31af3&query=${encodeURI(this.state.address)}`)
        console.log("DATA: ", data )
        if(data.status == 200)
            this.setState({places: data.data.data})*/
        const list = cities.filter(city => city.name.toLowerCase().includes(this.state.address.toLowerCase()))
        console.log(this.state.address, list);
        this.setState({places: list})
    }

    render() {
        return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-8 col-sm-12">
                    <InputGroup className="mb-3 mt-8" size="lg">
                        <FormControl
                            placeholder="Place, Address, City"
                            aria-label="address"
                            aria-describedby="basic-addon2"
                            onChange={e => this.addressChange(e)}
                        />
                        <InputGroup.Append>
                            <Button onClick={() => this.search()} variant="outline-secondary">Search</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </div>
            </div>
            <div className="row">{this.state.places.map((ele, index) => <PlaceCard index={index} place={ele} />)}</div>
        </div>
        )
    }
}