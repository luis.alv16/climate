import { Card, Button } from "react-bootstrap"
import Link from 'next/link'
import { useRouter } from 'next/router'

const PlaceCard = (props) => {

    console.log("PLACE: ", props.place)
    const {country, name, lat, lng } = props.place
    return (
        <div className="col-lg-6 col-sm-12 mt-3" key={props.index}><Card>
            <Card.Body>
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col">
                            <p>{name} - {country}</p>
                        </div>
                        <div className="col">
                            <img
                                alt={country}
                                style={{height: "40%", width: "40%"}}
                                src={`http://catamphetamine.gitlab.io/country-flag-icons/3x2/${country}.svg`}
                            />
                        </div>
                        <div className="col-md-auto">
                            <Link href={`/place?country=${country}&name=${name}&lat=${lat}&lng=${lng}`}>View Weather</Link>
                        </div>
                    </div>
                </div>
            </Card.Body>
        </Card></div>
    )
}

export default PlaceCard